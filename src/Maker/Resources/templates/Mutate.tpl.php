<?= "<?php\n"; ?>

namespace App\GraphQL\<?= $domainNSClassname; ?>;

use <?= $nsClassName; ?>;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Exception;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Overblog\GraphQLBundle\Definition\Resolver\MutationInterface;


class Mutate implements MutationInterface, AliasedInterface
{

    /**
     * @var \Doctrine\Common\Persistence\ObjectRepository|EntityRepository
     */
    private $repo;

    /**
     * @var $em EntityManager
     */
    private $em;

    /**
     * constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repo = $this->em->getRepository(<?= $className; ?>::class);
    }

    /**
     * @param array $args
     * @return array
     * @throws Exception
     */
    public function Create(array $args) : array
    {
        try {

            $<?= $varSingle; ?> = new <?= $className; ?>();

<?php foreach($fieldProperties as $fieldPropertyName => $fieldPropertyBag) {
    if(!$fieldPropertyBag["isPrimaryKey"]) {
?>
            $<?= $varSingle; ?>->set<?= Symfony\Bundle\MakerBundle\Str::asCamelCase($fieldPropertyBag["fieldName"]); ?>($args['<?= $fieldPropertyBag["fieldName"]; ?>']);
<?php
    }
}
?>

<?php foreach($relationships as $relationship) {
    if($relationship['plural']) {
?>
            if(isset($args['<?= $relationship['fkId']; ?>'])) {
                $<?= $relationship['varPlural']; ?> = $this->em->getRepository(\<?= $relationship['nsClassName']; ?>::class)->findBy(['id' => $args['<?= $relationship['fkId']; ?>']]);
                foreach($<?= $relationship['varPlural']; ?> as $<?= $relationship['varSingle']; ?>) {
                    $<?= $varSingle; ?>->add<?= $relationship['className'] ?>($<?= $relationship['varSingle']; ?>);
                }
            }
<?php
    } else {
?>
            if(isset($args['<?= $relationship['fkId']; ?>'])) {
                $<?= $relationship['varSingle']; ?> = $this->em->getRepository(\<?= $relationship['nsClassName']; ?>::class)->findOneBy(['id' => $args['<?= $relationship['fkId']; ?>']]);
                if($<?= $relationship['varSingle']; ?>) {
                    $<?= $varSingle; ?>->set<?= $relationship['className'] ?>($<?= $relationship['varSingle']; ?>);
                } else {
                    throw new Exception("Unknown <?= $relationship['human']; ?> Identifier");
                }
            }
<?php
    }
}
?>

            $this->em->persist($<?= $varSingle; ?>);
            $this->em->flush();

            return ['<?= $varSingle; ?>' => $<?= $varSingle; ?>];

        } catch (Exception $e) {
            throw new Exception("Failed to Create new <?= $human; ?>!");

        }

    }

    /**
     * @param string $id
     * @param array $args
     * @return array
     * @throws Exception
     */
    public function Edit(string $id, array $args) : array
    {
        try {

            /** @var <?= $className; ?> $<?= $varSingle; ?> */
            $<?= $varSingle; ?> = $this->repo->findOneBy(['id' => $id]);
<?php foreach($fieldProperties as $fieldPropertyName => $fieldPropertyBag) {
    if(!$fieldPropertyBag["isPrimaryKey"]) {
?>
            $<?= $varSingle; ?>->set<?= Symfony\Bundle\MakerBundle\Str::asCamelCase($fieldPropertyBag["fieldName"]); ?>($args['<?= $fieldPropertyBag["fieldName"]; ?>']);
<?php
    }
}
?>

<?php foreach($relationships as $relationship) {
    if($relationship['plural']) {
?>
            if(isset($args['<?= $relationship['fkId']; ?>'])) {
                $<?= $relationship['varPlural']; ?> = $this->em->getRepository(\<?= $relationship['nsClassName']; ?>::class)->findBy(['id' => $args['<?= $relationship['fkId']; ?>']]);
                if($<?= $relationship['varPlural']; ?>) {
                    foreach($<?= $varSingle; ?>->get<?= ucfirst(Symfony\Bundle\MakerBundle\Str::singularCamelCaseToPluralCamelCase($relationship["className"])); ?>() as $old<?= $relationship['className'] ?>){
                        $<?= $varSingle; ?>->remove<?= $relationship['className'] ?>($old<?= $relationship['className'] ?>);
                    }
                }
                foreach($<?= $relationship['varPlural']; ?> as $<?= $relationship['varSingle']; ?>) {
                    $<?= $varSingle; ?>->add<?= $relationship['className'] ?>($<?= $relationship['varSingle']; ?>);
                }
            }
<?php
    } else {
?>
            if(isset($args['<?= $relationship['fkId']; ?>'])) {
                $<?= $relationship['varSingle']; ?> = $this->em->getRepository(\<?= $relationship['nsClassName']; ?>::class)->findOneBy(['id' => $args['<?= $relationship['fkId']; ?>']]);
                if($<?= $relationship['varSingle']; ?>) {
                    $<?= $varSingle; ?>->set<?= ucfirst($relationship['field']) ?>($<?= $relationship['varSingle']; ?>);
                } else {
                    throw new Exception("Unknown <?= $relationship['human']; ?> Identifier");
                }
            }
<?php
    }
}
?>

            $this->em->persist($<?= $varSingle; ?>);
            $this->em->flush();

            return ['<?= $varSingle; ?>' => $<?= $varSingle; ?>];

        } catch (Exception $e) {
            throw new Exception("Failed to Update <?= $className; ?> Item!");

        }

    }

    /**
     * @param array $ids
     * @return array
     * @throws Exception
     */
    public function Delete(array $ids) : array
    {
        $removed = [];
        $<?= $varSingle; ?>Items = $this->repo->findBy(['id' => $ids]);

        foreach($<?= $varSingle; ?>Items as $item) {
            $toRem = $item->getId();
            $this->em->remove($item);
            $removed[] = $toRem;
        }

        $this->em->flush();

        return ['ids' => $removed];

    }

    /**
     * @return array
     */
    public static function getAliases()
    {
        return [
            'Create'    => '<?= $snake_class_name; ?>_create',
            'Edit'      => '<?= $snake_class_name; ?>_edit',
            'Delete'    => '<?= $snake_class_name; ?>_multi_delete'
        ];
    }
}