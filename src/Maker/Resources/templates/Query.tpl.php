<?= "<?php\n"; ?>

namespace App\GraphQL\<?= $domainNSClassname; ?>;

use <?= $nsClassName; ?>;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Overblog\GraphQLBundle\Definition\Resolver\ResolverInterface;

class Query implements ResolverInterface, AliasedInterface
{

    /**
     * @var \Doctrine\Common\Persistence\ObjectRepository|EntityRepository
     */
    private $repo;

    /**
     * @var $em EntityManager
     */
    private $em;

    /**
     * constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repo = $this->em->getRepository(<?= $className; ?>::class);
    }

    /**
     * @param string $id
     * @return <?= $className; ?>|null|object
     */
    public function One(string $id): ?<?= $className; ?>
    {
        $<?= $varSingle; ?> = $this->repo->findOneBy(['id' => $id]);

        return $<?= $varSingle; ?>;
    }

    /**
     * @param int $limit
     * @return array
     */
    public function Collection(int $limit): array
    {
        $<?= $varPlural; ?> = $this->repo->findBy([], ['id' => 'desc'], $limit, 0);
        return ['<?= $varPlural; ?>' => $<?= $varPlural; ?>];
    }

    /**
     * @return array
     */
    public static function getAliases(): array
    {
        return  [
            'One'   => '<?= $snake_class_name; ?>',
            'Collection'  => '<?= $snake_class_name; ?>_collection'
        ];
    }
}