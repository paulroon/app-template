<?php


namespace App\Maker;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use Symfony\Bundle\MakerBundle\ConsoleStyle;
use Symfony\Bundle\MakerBundle\DependencyBuilder;
use Symfony\Bundle\MakerBundle\Doctrine\ORMDependencyBuilder;
use Symfony\Bundle\MakerBundle\Generator;
use Symfony\Bundle\MakerBundle\InputAwareMakerInterface;
use Symfony\Bundle\MakerBundle\InputConfiguration;
use Symfony\Bundle\MakerBundle\Maker\AbstractMaker;
use Symfony\Bundle\MakerBundle\Str;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml;

/**
 */
final class MakeResource extends AbstractMaker implements InputAwareMakerInterface
{

    const ENTITY_ROOT_NS = "App\\Entity\\";
    /**
     * @var EntityManager
     */
    private $entityManager;

    /** @var array $entityMetaData */
    private $entityMetaData;

    /** @var array $generationList */
    private $generationList;

    /** @var array $paths */
    private $paths;

    /** @var array $yamlUpdateQueue */
    private $yamlUpdateQueue;


    /**
     * MakeResource constructor.
     * @param EntityManager $entityManager
     * @param ContainerInterface $container
     */
    public function __construct(EntityManager $entityManager, ContainerInterface $container)
    {
        $this->paths['appRootDir'] = $container->getParameter('kernel.project_dir');
        $this->paths['gqlConfigRootDir'] = sprintf('%s/config/graphql/types', $this->paths['appRootDir']);
        $this->paths['tplResourceDir'] = sprintf('%s/src/Maker/Resources/templates', $this->paths['appRootDir']);
        $this->getEntityMetaData($entityManager);
        $this->generationList = $this->yamlUpdateQueue = [];
    }

    /**
     * Return the command name for your maker (e.g. make:report).
     *
     * @return string
     */
    public static function getCommandName(): string
    {
        return 'make:resource';
    }

    /**
     * Configure the command: set description, input arguments, options, etc.
     *
     * By default, all arguments will be asked interactively. If you want
     * to avoid that, use the $inputConfig->setArgumentAsNonInteractive() method.
     *
     * @param Command $command
     * @param InputConfiguration $inputConfig
     */
    public function configureCommand(Command $command, InputConfiguration $inputConfig)
    {
        $command
            ->setDescription('Creates GQL Resources from Doctrine')
            ->addOption('all', 'a', InputOption::VALUE_NONE, 'Call --all to iterate through ALL entities creating resource for each.')
            ->addOption('dry-run', null, InputOption::VALUE_NONE, 'Call --dry-run to run the script without writing any output to disk.')
            ->setHelp(file_get_contents(__DIR__.'/Resources/help/MakeResource.txt'))
        ;

    }

    /**
     * Configure any library dependencies that your maker requires.
     *
     * @param DependencyBuilder $dependencies
     * @param InputInterface|null $input
     */
    public function configureDependencies(DependencyBuilder $dependencies, InputInterface $input = null)
    {
        $dependencies->requirePHP71();

        ORMDependencyBuilder::buildDependencies($dependencies);
    }

    /**
     * @param InputInterface $input
     * @param ConsoleStyle $io
     * @param Command $command
     */
    public function interact(InputInterface $input, ConsoleStyle $io, Command $command)
    {

        if(!$this->entityMetaData) {
            $io->error("There are no Doctrine Entities available for resource mapping.");
            $io->note("You can add Entities via make:entity [ClassName]");
            $io->success("Bye!");
            die;
        }

        if(!$input->getOption('all')) {
            $entityClassName = $this->selectEntity($io)->getName();
            $this->generationList[] = $entityClassName;
        }else {
            $this->generationList = array_keys($this->entityMetaData);
        }

    }

    /**
     * @param InputInterface $input
     * @param ConsoleStyle $io
     * @param Generator $generator
     * @throws \Exception
     */
    public function generate(InputInterface $input, ConsoleStyle $io, Generator $generator)
    {
        foreach ($this->generationList as $entityClassName) {

            $io->comment(sprintf("Generating [<fg=yellow>%s</>]", $entityClassName));

            $this->buildGQLTypeDefinition($entityClassName, $generator);

            $this->buildResolverClasses($entityClassName, $generator);

        }

        $this->commitToDisk($input, $io, $generator);

        $io->success("complete");
    }

    private function commitToDisk(InputInterface $input, ConsoleStyle $io, Generator $generator)
    {
        if(!$input->getOption('dry-run')) {

            $generator->writeChanges();

            foreach ($this->yamlUpdateQueue as $file => $data) {
                file_put_contents($file, Yaml::dump($data, 4, 2));
            }
        }else {
            $io->success("complete - With nothing written to disk (no changes applied)");
            die;
        }

    }


    private function getEntityMetaData(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;

        /** @var ClassMetadata $meta */
        foreach ($this->entityManager->getMetadataFactory()->getAllMetadata() as $meta) {
            $this->entityMetaData[$meta->getName()] = $meta;
        }
    }

    private function selectEntity(ConsoleStyle $io): ClassMetadata
    {

        $name = $io->choice(
            'Select an Entity',
            array_keys($this->entityMetaData),
            array_keys($this->entityMetaData)[0]
        );

        return $this->entityMetaData[$name];
    }

    private function buildGQLTypeDefinition($entityClassName, Generator $generator)
    {

        $data = $this->getEntityFieldDataForGql($entityClassName);

        $outputTypeDefinitionFilename = sprintf(
            '%s/%s%s.types.yaml',
            $this->paths['gqlConfigRootDir'],
            (!empty($data['domain'])) ? $data['domain'] . '/' : '',
            $data['snake_class_name']
        );

        // Overblog GraphQL type definition (config)
        //
        $generator->generateFile(
            $outputTypeDefinitionFilename,
            sprintf('%s/__TYPE__.types.tpl.yaml', $this->paths['tplResourceDir']),
            $data
        );

        // Ensure RootQuery inherits new Query definitions
        //
        $this->addNewGQLQueryDaughter($data['className']);


        return $outputTypeDefinitionFilename;

    }

    private function addNewGQLQueryDaughter(string $daughterClassName)
    {
        foreach(['query', 'mutation'] as $type){

            $genQueryYamlFile = sprintf('%s/%s.types.yaml', $this->paths['gqlConfigRootDir'], strtolower($type));

            if(isset($this->yamlUpdateQueue[$genQueryYamlFile])) {
                $queryTypeYaml = $this->yamlUpdateQueue[$genQueryYamlFile];
            } else {
                $queryTypeYaml = Yaml::parseFile($genQueryYamlFile);
            }

            if(!isset($queryTypeYaml[ucfirst($type)])) {

                $queryTypeYaml[ucfirst($type)] = ['inherits' => []];

            }else if(!isset($queryTypeYaml[ucfirst($type)]['inherits'])) {

                $queryTypeYaml[ucfirst($type)]['inherits'] = [];

            }

            $queryTypeYaml[ucfirst($type)]['inherits'][] = Str::addSuffix($daughterClassName, ucfirst($type));

            $this->yamlUpdateQueue[$genQueryYamlFile] = $queryTypeYaml;
        }

    }

    private function getEntityFieldDataForGql(string $entityClassName)
    {
        return $this->getTemplatingVars($entityClassName, [
            'type' => function($fMapping){
                return $this->doctrineToGqlScalarType($fMapping["type"], $fMapping["nullable"]);
            }
        ]);
    }

    private function getTemplatingVars(string $entityClassName, array $mappingFns = [])
    {

        /** @var ClassMetadata $entityClassMetaData */
        $entityClassMetaData = $this->entityMetaData[$entityClassName];

        $properties = [];
        foreach($entityClassMetaData->getFieldNames() as $field) {

            $fMapping = $entityClassMetaData->getFieldMapping($field);

            foreach(["type", "unique", "nullable", "columnName"] as $mapFieldDataItem) {
                $properties[$field][$mapFieldDataItem] = isset($mappingFns[$mapFieldDataItem])
                    ? $mappingFns[$mapFieldDataItem]($fMapping)
                    : $fMapping[$mapFieldDataItem];
            }
            $properties[$field]["fieldName"] = isset($mappingFns["fieldName"]) ? $mappingFns["fieldName"]($field) : $field;
            $properties[$field]['isPrimaryKey'] = (isset($fMapping["id"]) && $fMapping["id"]);

        }


        $relationships = [];
        foreach ($entityClassMetaData->getAssociationMappings() as $field => $relData) {

                $relationships[] = $this->addClassNameVariations($relData['targetEntity'], [
                    'field' => $relData['fieldName'],
                    'targetEntity' => $relData['targetEntity'],
                    'isOwningSide' => $relData['isOwningSide'],
                    'inversedBy' => $relData['inversedBy'],
                    'type' => $this->mapRelationTypeToString($relData['type']),
                    'gqlType' => $this->getGQLTypeName($relData['targetEntity'], $relData['type']),
                    'gqlIDType' => $this->getGQLIDTypeName($relData['type']),
                    'plural' => $this->isTypePluralForLocal($relData['type']),
                    'fkId' => $this->getFkId($relData['targetEntity'], $relData['type'])
                ]);
        }

        return $this->addClassNameVariations($entityClassName, [
            "fieldProperties" => $properties,
            "relationships" => $relationships
        ]);
    }

    private function addClassNameVariations(string $entityClassName, array $assocData = [])
    {
        $assocData['domain']            = $this->getDomainFromClassName($entityClassName);
        $assocData['nsClassName']       = Str::asClassName($entityClassName);
        $assocData['domainNSClassname'] = str_replace(self::ENTITY_ROOT_NS, '', $assocData['nsClassName']);
        $assocData['namespace']         = Str::getNamespace($entityClassName);
        $assocData['className']         = Str::getShortClassName($entityClassName);
        $assocData['varSingle']         = Str::pluralCamelCaseToSingular(Str::getShortClassName($entityClassName));
        $assocData['varPlural']         = Str::singularCamelCaseToPluralCamelCase(Str::getShortClassName($entityClassName));
        $assocData['human']             = trim(Str::asHumanWords(Str::getShortClassName($entityClassName)));
        $assocData['snake_class_name']  = str::asSnakeCase(Str::getShortClassName($entityClassName));

        return $assocData;

    }

    private function getFkId($entityClassName, $type): String
    {
        return sprintf(
            "%s_id%s",
            Str::pluralCamelCaseToSingular(Str::getShortClassName($entityClassName)),
            $this->isTypePluralForLocal($type) ? 's': ''
        );
    }
    private function getGQLTypeName($entityClassName, $type): String
    {
        return $this->isTypePluralForLocal($type)
            ? sprintf("[%s]", Str::getShortClassName($entityClassName))
            : Str::getShortClassName($entityClassName);
    }

    private function getGQLIDTypeName($type): String
    {
        return $this->isTypePluralForLocal($type) ? "[ID]" : "ID";
    }

    private function isTypePluralForLocal($type)
    {
        return in_array($type, [
            ClassMetadataInfo::ONE_TO_MANY,
            ClassMetadataInfo::MANY_TO_MANY
        ]);
    }

    /**
     * @param $type
     * @return string|null
     */
    private function mapRelationTypeToString($type): ?string
    {
        switch ($type) {
            case ClassMetadataInfo::ONE_TO_ONE: return 'OneToOne';
            case ClassMetadataInfo::MANY_TO_ONE: return 'ManyToOne';
            case ClassMetadataInfo::ONE_TO_MANY: return 'OneToMany';
            case ClassMetadataInfo::MANY_TO_MANY: return 'ManyToMany';
            default: return null;
        }
    }

    /**
     * Map a Doctrine scalar type to a GQL definition lang type
     * @param $type
     * @param bool $nullable
     * @return string
     */
    private function doctrineToGqlScalarType($type, $nullable = false)
    {
        switch (strtolower($type)) {
            case "uuid": case "guid":
                $mappedTypeName = "ID";
                break;
            case "integer": case "smallint": case "bigint":
                $mappedTypeName = "Int";
                break;
            case "float": case "decimal":
                $mappedTypeName = "Float";
                break;
            case "boolean":
                $mappedTypeName = "Boolean";
                break;
            case "date": case "datetime":
                $mappedTypeName = "String";
                break;
            case "string":
            default:
                $mappedTypeName = "String";
                break;
        }

        return sprintf("%s%s", $mappedTypeName,  ($nullable) ? "": "!");
    }

    private function buildResolverClasses($entityClassName, Generator $generator)
    {
        $data = $this->getTemplatingVars($entityClassName);

        $ns = (!empty($data['domain']))
            ? "GraphQL\\" . $data['domain'] . "\\" . $data['className']
            : "GraphQL\\" . $data['className'];

        foreach (["Mutate", "Query"] as $sType) {
            $tplPath = sprintf("%s/%s.tpl.php", $this->paths['tplResourceDir'], $sType);
            $this->buildClass($sType, $ns, $data, $tplPath, $generator);
        }
    }

    private function buildClass($className, $ns, array $data, string $template, Generator $generator)
    {

        $readerClassNameDetails = $generator->createClassNameDetails($className, $ns);

        $generator->generateClass(
            $readerClassNameDetails->getFullName(),
            $template,
            $data
        );
    }

    private function getDomainFromClassName($entityClassName)
    {
        $domainNSArr = explode("\\", str_replace(self::ENTITY_ROOT_NS, "", $entityClassName));
        array_pop($domainNSArr);
        return implode("\\", $domainNSArr);
    }

}