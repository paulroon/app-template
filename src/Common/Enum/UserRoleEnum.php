<?php


namespace App\Common\Enum;

/**
 * Class UserRoleEnum
 *
 * Enum Role names for User Roles
 * @package App\Common\Enum
 */
final class UserRoleEnum
{
    const ROLE_USER = "ROLE_USER";
    const ROLE_ADMIN = "ROLE_ADMIN";
}