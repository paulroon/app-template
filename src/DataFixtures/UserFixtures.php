<?php

namespace App\DataFixtures;

use App\Common\Enum\UserRoleEnum;
use App\Entity\User\User;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends BaseFixtures
{

    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    protected function loadData(ObjectManager $manager)
    {
        /**
         * ADMIN
         */
        $admin = new User();
        $admin
            ->setEmail("admin@admin.com")
            ->setRoles([UserRoleEnum::ROLE_USER, UserRoleEnum::ROLE_ADMIN])
            ->setFirstname("King")
            ->setLastname("Administrator");

        $admin->setPassword($this->encoder->encodePassword(
            $admin,
            'badger'
        ));

        $this->addReference("adminUser", $admin);

        $manager->persist($admin);

        /**
         * USER(s)
         */
        $this->createMany(User::class, 10, function($entity, $count) {
            /** User $entity */
            $entity
                ->setEmail(sprintf("user_%s@example.com", $count))
                ->setFirstname($this->faker->firstName)
                ->setLastname($this->faker->lastName)
                ->setRoles([UserRoleEnum::ROLE_USER])
            ;

            $entity->setPassword($this->encoder->encodePassword(
                $entity,
                'badger'
            ));
        });


        $manager->flush();
    }
}
