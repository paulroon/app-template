var Encore = require('@symfony/webpack-encore');

Encore
    .setOutputPath('public/build/')

    .setPublicPath('/build')

    .addEntry('app', [
        './public/assets/js/global.js',
        './public/assets/js/theme.js'
    ])
    .addEntry('security', './public/assets/js/security.js')
    .addEntry('landing', './public/assets/js/landing.js')

    .enableBuildNotifications()

    .enableSingleRuntimeChunk()

    .autoProvidejQuery()
;

// export the final configuration
module.exports = Encore.getWebpackConfig();