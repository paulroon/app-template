App Template
====================

## Overview

The App-Template is a fast way to spin up a new Enterprise Application using the following stack. 
 - Docker
 - PHP 7 (Symfony 4)
 - MySQL 8+
 - GraphQL (using overblog vendor package)
 
 It will come with some built in tools for creating resources, (Doctrine Entities + GQL config for CRUD operations)
 
 Its not yet a stable build and as such shouldn't be used but is perhaps demonstrative of a solid back-end platform design.

####  Dependencies

You will need docker & docker-compose installed

#### Install

```bash
 $> cd ~/[DEV_FOLDER]

 $> git clone git@bitbucket.org:paulroon/coin-vending-machine.git

 $> cd ./app-template
```

#### Start platform
 There is a custom script for building the platform... 
```bash
 $> ./bin/dev/start 
```

### Add some Resources
You will need to make some normal doctrine entities for your resources, 
You can do this by hand.. or use the 'make' command
```bash
 $> ./bin/dev/console make:entity
``` 
 ... follow the steps
 then create the migrations.. 
 
```bash
 $> ./bin/dev/console make:migration
 $> ./bin/dev/console doctrine:migrations:migrate 
```

now you can use the custom command for generating the GQL config
```bash
 $> ./bin/dev/console make:resources --all
```
it can handle relationships

#### Load data fixtures
 Load the data fixtures 
```bash
 $> ./bin/dev/console doctrine:fixtures:load 
```


#### Have a look
There's not much to see yet except a home screen and some login pages. 

http://localhost:8000/

The data fixtures come with a few 'out of the box' dev users.. try admin@admin.com / (password: badger)

### Front end
currently there just some twig based boiler plate here.. 
There will eventually be a React based SPA for an authenticated Dashboard that scaffolds basic crud based on vailable entities / GQL endpoints